import React, { Component } from 'react'
import api from '../../services/api'

import { distanceInWords } from 'date-fns'
import pt from 'date-fns/locale/pt'

import Dropzone from 'react-dropzone'
import socket from 'socket.io-client'

import { MdInsertDriveFile } from 'react-icons/md'

import logo from '../../assets/logo.png'
import './styles.css'

class Box extends Component {

    state = {
        box: {}
    }

    async componentDidMount() {
        this.subscribeToNewFiles();

        const idbox = this.props.match.params.id;
        const res = await api.get('/boxes/' + idbox)

        this.setState({
            box: res.data
        })
    }

    subscribeToNewFiles = () => {
        const idbox = this.props.match.params.id;

        // baseURL: 'https://oministack-files-backend.herokuapp.com'
        // baseURL: 'http://localhost:3333'

        const io = socket('http://localhost:3333')
        io.emit('connectRoom', idbox);

        io.on('file', data => {
            this.setState({
                box: {
                    ...this.state.box,
                    files: [data, ...this.state.box.files]
                }
            })
        })
    }

    handleUpload = (files) => {
        const idbox = this.props.match.params.id;

        files.forEach(f => {
            const formulario = new FormData();
            formulario.append('file', f);
            api.post('boxes/' + idbox + '/files', formulario)
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <img src={logo} alt="" />
                    <h1>{this.state.box.title}</h1>
                </header>

                <Dropzone onDropAccepted={this.handleUpload}>
                    {({ getRootProps, getInputProps }) => (
                        <div className="upload" {...getRootProps()}>
                            <input {...getInputProps()} />
                            <p>Arraste arquivos ou clique aqui</p>
                        </div>
                    )}
                </Dropzone>

                <ul>
                    {this.state.box.files && this.state.box.files.map((f, i) => {
                        <li key={i}>
                            <a className="fileInfo" href={f.url} target="_blank">
                                <MdInsertDriveFile size={24} color="#A5Cfff" />
                                <b>{f.title}</b>
                            </a>

                            <span>há {distanceInWords(f.createdAt, new Date(), { locale: pt })}</span>
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

export default Box
