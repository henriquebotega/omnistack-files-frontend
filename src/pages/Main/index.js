import React, { Component } from 'react'

import api from '../../services/api'

import logo from '../../assets/logo.png'
import './styles.css'

class Main extends Component {

    state = {
        newBox: ''
    }

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = async (e) => {
        e.preventDefault();

        const res = await api.post('/boxes', {
            title: this.state.newBox
        })

        this.props.history.push('/box/' + res.data._id)
    }

    render() {
        return (
            <div id="main-container">
                <form onSubmit={this.handleSubmit}>
                    <img src={logo} alt="" />
                    <input type="text" placeholder="Criar um box" name='newBox' value={this.state.newBox} onChange={this.handleInputChange} />
                    <button type="submit">Criar</button>
                </form>
            </div>
        )
    }
}

export default Main
